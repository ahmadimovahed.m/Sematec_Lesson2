package com.myproject_lesson2.m.myproject_lesson2.models;

/**
 * Created by M on 11/10/2017.
 */

public class MenuModel {
     String MenuName;
     String GotoPage;
    String ImgUrl;

    public String getImgUrl() {
        return ImgUrl;
    }

    public void setImgUrl(String imgUrl) {
        ImgUrl = imgUrl;
    }

    public MenuModel(String menuName, String gotoPage ,String imgUrl) {
        MenuName = menuName;
        GotoPage = gotoPage;
        ImgUrl=imgUrl;
    }

    public String getMenuName() {
        return MenuName;
    }

    public void setMenuName(String menuName) {
        MenuName = menuName;
    }

    public String getGotoPage() {
        return GotoPage;
    }

    public void setGotoPage(String gotoPage) {
        GotoPage = gotoPage;
    }
}
