package com.myproject_lesson2.m.myproject_lesson2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.myproject_lesson2.m.myproject_lesson2.adapters.foodListAdapter;
import com.myproject_lesson2.m.myproject_lesson2.models.FoodModel;

import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {
    ListView foodsListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        bindView();
        List<FoodModel> listFoodModel=new ArrayList<FoodModel>();
        FoodModel model1=new FoodModel("Cheeseburger",23000,"https://www.bodybuilding.com/fun/images/2014/in-defense-of-processed-foods-graphics-1.jpg");
        FoodModel model2=new FoodModel("Pizza",280000,"http://i.dailymail.co.uk/i/pix/2016/07/28/19/36ADC69D00000578-3695870-image-a-15_1469729356684.jpg");
        FoodModel model3=new FoodModel("Chicken",58000 ,"http://cdn.cnn.com/cnnnext/dam/assets/111122095737-thanksgiving-dish-turkey-horizontal-large-gallery.jpg");
        FoodModel model4=new FoodModel("Steak",780000,"https://www.retaildetail.eu/sites/default/files/news/shutterstock_138421859.jpg");
        FoodModel model5=new FoodModel("Sandwich",450000,"https://www.cheapoair.com/miles-away/wp-content/uploads/2016/01/Hot-Dogs.jpg");

        listFoodModel.add(model1);
        listFoodModel.add(model2);
        listFoodModel.add(model3);
        listFoodModel.add(model4);
        listFoodModel.add(model5);
        listFoodModel.add(model1);
        listFoodModel.add(model2);
        listFoodModel.add(model3);
        listFoodModel.add(model4);
        listFoodModel.add(model5);
        listFoodModel.add(model1);
        listFoodModel.add(model2);
        listFoodModel.add(model3);
        listFoodModel.add(model4);
        listFoodModel.add(model5);
        foodListAdapter adapter=new foodListAdapter(this,listFoodModel);
        foodsListView.setAdapter(adapter);

        foodsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                FoodModel clickItem=(FoodModel)adapterView.getItemAtPosition(i);
                String aa=clickItem.getFoodName().toString();
                Toast.makeText(ListViewActivity.this,"You Click at:"+aa.toString(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    void bindView() {
         foodsListView = (ListView) findViewById(R.id.foodsListView);
    }
}
