package com.myproject_lesson2.m.myproject_lesson2;

import android.app.ProgressDialog;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.myproject_lesson2.m.myproject_lesson2.models.MaadrianProductmodel;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.Header;

import static com.squareup.picasso.Picasso.*;

public class WebServiceActivity extends AppCompatActivity implements View.OnClickListener {
EditText txtSerach;
Button btnSeach;
TextView productName;
TextView startDate;
TextView endDate;
TextView  serialNo;
TextView productGroupName;
TextView result;
String ProductCode;
ProgressDialog dialog;
ImageView ImgMaadiranLogo;
ImageView imgProduct;
String UrlImg;
Boolean backClicked=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_service);
        bind();
        dialog=new ProgressDialog(this);
        dialog.setTitle("Waiting..");
        dialog.setMessage("Please Wait for Maadiran Response");


    }
















    public void bind(){
        txtSerach=( EditText)findViewById(R.id.txtSerach);
         btnSeach=(Button)findViewById(R.id.btnSerach);
         productName=(TextView)findViewById(R.id.productName);

        startDate=(TextView)findViewById(R.id.startDate);
        endDate=(TextView)findViewById(R.id.endDate);
        productGroupName=(TextView)findViewById(R.id.productGroupName);
        serialNo=(TextView)findViewById(R.id.serialNo);
        result=(TextView)findViewById(R.id.result);

         btnSeach.setOnClickListener(this);
         ImgMaadiranLogo=(ImageView)findViewById(R.id.ImgMaadiranLogo);
        with(this).load("http://maadiran.com/MasterPageImages/NavigationHeader/logo.png").into(ImgMaadiranLogo);
        imgProduct=(ImageView)findViewById(R.id.imgProduct);
    }

    @Override
    public void onClick(View view) {

        if(view.getId()==R.id.btnSerach){

            ProductCode=txtSerach.getText().toString();
          //  getDataFromMaaadiran( ProductCode);
            GetDataFromMaadiranAsync( ProductCode);

        }
    }


    public void getDataFromMaaadiran(final String ValidateCode){


        new Thread(new Runnable() {public void run() {

            try {
                String url="http://wgs.maadiran.co.ir/api/GetProductInfo?validationcode="+ValidateCode;
                URL u = new URL(url);
                HttpURLConnection con = (HttpURLConnection) u.openConnection();
                con.setRequestMethod("GET");
                if (con.getResponseCode() == 200) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String InputLine;
                    StringBuffer response = new StringBuffer();
                    while ((InputLine = in.readLine()) != null) {
                        response.append(InputLine);
                    }
                    GetFromJson(response.toString());


                }

            }
            catch (Exception e){
                Log.d("rest_api error: ", e + "");

            }

        }}).start();


    }

    public void GetFromJson(String Responce){
        try {
            JSONObject objAll=new JSONObject(Responce);
            final String ProductNameV=objAll.getString("ProductName");

            final String StartDateV=objAll.getString("StartDate");
            final String EndDateV=objAll.getString("EndDate");
            final String ProductGroupNameV=objAll.getString("ProductGroupName");

            final String SerialNoV=objAll.getString("SerialNo");
            final String ValidationCodeV=objAll.getString("ValidationCode");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(SerialNoV!="null") {
                        result.setText("Validate Code" );
                        serialNo.setText("SerialNo: " + SerialNoV.toString());
                        productName.setText("ProductName: " + ProductNameV.toString());
                        startDate.setText("StartDate: " + StartDateV.toString());
                        endDate.setText("EndDate: " + EndDateV.toString());
                        productGroupName.setText("ProductGroupName: " + ProductGroupNameV.toString());

try {

    if (ValidationCodeV == "114077620") {
        UrlImg = "http://www.maadiran.com/ProductDetails.aspx?ProID=1371";
    }
    if (ValidationCodeV == "111550707") {
        UrlImg = "http://www.maadiran.com/Product_Images/ProductImage_Scanner_PerfectionV19Photo_1281ID_1.jpg";
    }


    with(WebServiceActivity.this).load(UrlImg).into(imgProduct);
}
catch (Exception e){
    Log.e("rest_api error: ", e + "");
}
                    }
                    else
                        {
                            result.setText("Your Code Is Not Valid " );
                            serialNo.setText("" );
                            productName.setText("");
                            startDate.setText("" );
                            endDate.setText("" );
                            productGroupName.setText("" );
                        }
                }
            });
        }
        catch (Exception e){
            Log.d("rest_api error: ", e + "");
        }


    }


    public void GetDataFromMaadiranAsync(String ValidateCode){
        String url="http://wgs.maadiran.co.ir/api/GetProductInfo?validationcode="+ValidateCode;
        AsyncHttpClient client=new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(WebServiceActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
               // GetFromJson(responseString);
                GetFromGson(responseString);

            }

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                dialog.dismiss();
                Toast.makeText(WebServiceActivity.this,"success",Toast.LENGTH_SHORT).show();

            }
        });

    }


    public void GetFromGson(String Responce){
        try {


            Gson gson = new Gson();

            MaadrianProductmodel Pmodel = gson.fromJson(Responce, MaadrianProductmodel.class);
            if (Pmodel.getSerialNo().toString() != "null") {


                result.setText("Validate Code");
                serialNo.setText("SerialNo: " + Pmodel.getSerialNo().toString());
                productName.setText("ProductName: " + Pmodel.getProductName().toString());
                startDate.setText("StartDate: " + Pmodel.getStartDate().toString());
                endDate.setText("EndDate: " + Pmodel.getEndDate().toString());
                productGroupName.setText("ProductGroupName: " + Pmodel.getProductGroupName().toString());
                UrlImg = "http://www.maadiran.com/ProductDetails.aspx?ProID=1371";
                if (Pmodel.getValidationCode().toString() == "114077620") {
                    UrlImg = "http://www.maadiran.com/ProductDetails.aspx?ProID=1371";
                }
                if (Pmodel.getValidationCode().toString() == "111550707") {
                    UrlImg = "http://www.maadiran.com/Product_Images/ProductImage_Scanner_PerfectionV19Photo_1281ID_1.jpg";
                }


                with(WebServiceActivity.this).load(UrlImg).into(imgProduct);
            } else {
                result.setText("Your Code Is Not Valid ");
                serialNo.setText("");
                productName.setText("");
                startDate.setText("");
                endDate.setText("");
                productGroupName.setText("");
            }
        }
        catch (Exception e){
            result.setText("Your Code Is Not Valid ");
            serialNo.setText("");
            productName.setText("");
            startDate.setText("");
            endDate.setText("");
            productGroupName.setText("");
        }

    }


    @Override

    public void onBackPressed() {
        if(backClicked)
            finish();
        else{
            backClicked=true;
            Toast.makeText(WebServiceActivity.this,"Press Back Again ..",Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    backClicked=false;
                }
            },3000);
        }

    }
}
