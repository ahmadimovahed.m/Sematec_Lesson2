package com.myproject_lesson2.m.myproject_lesson2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class BrowserActivity extends AppCompatActivity implements View.OnClickListener {
   EditText txtUrlBrowser;

   WebView webviewBrowser;
    String searchUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);
        bind();
        findViewById(R.id.btnSerachBrowser).setOnClickListener(this);

        txtUrlBrowser.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                searchUrl=txtUrlBrowser.getText().toString();
                if(!searchUrl.startsWith("http://")){
                    searchUrl="http://"+searchUrl;
                }
                webviewBrowser.loadUrl(searchUrl.toString());



                return false;
            }
        });

    }

    private void bind() {
        txtUrlBrowser=(EditText)findViewById(R.id.txtUrlBrowser);
        webviewBrowser=(WebView)findViewById(R.id.webviewBrowser);
        webviewBrowser.getSettings().setJavaScriptEnabled(true);
        webviewBrowser.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.btnSerachBrowser){
             searchUrl=txtUrlBrowser.getText().toString();
            if(!searchUrl.startsWith("http://")){
                searchUrl="http://"+searchUrl;
            }
            webviewBrowser.loadUrl(searchUrl.toString());

        }
    }
}
