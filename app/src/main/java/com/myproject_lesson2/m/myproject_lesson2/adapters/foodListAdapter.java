package com.myproject_lesson2.m.myproject_lesson2.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.myproject_lesson2.m.myproject_lesson2.MainActivity;
import com.myproject_lesson2.m.myproject_lesson2.R;
import com.myproject_lesson2.m.myproject_lesson2.models.FoodModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by M on 10/31/2017.
 */

public class foodListAdapter extends BaseAdapter {
    Context mcontext;
    List<FoodModel> listFoods;

    public foodListAdapter(Context mcontext, List<FoodModel> listFoods) {
        this.mcontext = mcontext;
        this.listFoods = listFoods;
    }


    @Override
    public int getCount() {
        return listFoods.size();
    }

    @Override
    public Object getItem(int position) {
        return listFoods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView= LayoutInflater.from(mcontext).inflate(R.layout.foods_list_item,viewGroup,false);
        TextView foodName=(TextView)rowView.findViewById(R.id.foodName);
        TextView foodPrice=(TextView)rowView.findViewById(R.id.foodPrice);
        ImageView foodImg=(ImageView)rowView.findViewById(R.id.foodImg);

        foodName.setText(listFoods.get(position).getFoodName());
        foodPrice.setText(listFoods.get(position).getFoodPrice()+"");
        Picasso.with(mcontext).load(listFoods.get(position).getImgUrl()).into(foodImg);
        return rowView;

    }
}
