package com.myproject_lesson2.m.myproject_lesson2;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;




public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText FirstName;
    EditText LastName;
    EditText Result;
    Button BtnShow;
    Button BtnSave;
   // CheckBox Married;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        ImageView imgshow1=(ImageView)findViewById(R.id.Imgshow1);
//        Picasso.with(MainActivity.this).load("http://file.digi-kala.com/digikala/image/webstore/banner/1396/8/6/32d8498d.jpg").into(imgshow1);

        BindView();

    }



    public void BindView(){
        FirstName=(EditText)findViewById(R.id.FirstName);
        LastName=(EditText)findViewById(R.id.LastName);
        Result=(EditText)findViewById(R.id.Result);
        BtnSave=(Button)findViewById(R.id.BtnSave);
        BtnShow=(Button)findViewById(R.id.BtnShow);
        findViewById(R.id.BtnShow).setOnClickListener(this);
        findViewById(R.id.BtnSave).setOnClickListener(this);
       // findViewById(R.id.BtnNextPage).setOnClickListener(this);
       // Married=(CheckBox) findViewById(R.id.Married);

    }


    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.BtnSave){
            setShare("FirstName",FirstName.getText().toString());
            setShare("LastName",LastName.getText().toString());

            Result.setText("");
            Toast.makeText(this,"Success!",Toast.LENGTH_SHORT).show();
        }
        if (view.getId()==R.id.BtnShow){
          String ResultValue= getShare("FirstName","NoName")+"-"+
            getShare("LastName","NoFamily");

            Result.setText(ResultValue.toString());
        }
//        if (view.getId()==R.id.BtnNextPage)
//        {
//            Intent intent=new Intent(MainActivity.this,ListViewActivity.class);
//
//            startActivity(intent);
//
//        }

    }


    public  void setShare(String key,String value){


            PreferenceManager.getDefaultSharedPreferences(this).edit().putString(key, value).apply();

    }
    public  String getShare(String key,String defaultValue){
       return PreferenceManager.getDefaultSharedPreferences(this).getString(key,defaultValue);

    }
}
