package com.myproject_lesson2.m.myproject_lesson2.models;

/**
 * Created by M on 10/31/2017.
 */

public class FoodModel {
    String FoodName;
    int  FoodPrice;
    String ImgUrl;


    public FoodModel(String foodName, int foodPrice, String imgUrl) {
        this.FoodName = foodName;
        this.FoodPrice = foodPrice;
        this.ImgUrl = imgUrl;
    }

    public String getFoodName() {
        return FoodName;
    }

    public void setFoodName(String foodName) {
        this.FoodName = foodName;
    }

    public int getFoodPrice() {
        return FoodPrice;
    }

    public void setFoodPrice(int foodPrice) {
        this.FoodPrice = foodPrice;
    }

    public String getImgUrl() {
        return ImgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.ImgUrl = imgUrl;
    }
}
