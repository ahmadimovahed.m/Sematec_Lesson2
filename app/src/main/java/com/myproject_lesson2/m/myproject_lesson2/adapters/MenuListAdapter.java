package com.myproject_lesson2.m.myproject_lesson2.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.myproject_lesson2.m.myproject_lesson2.MainActivity;
import com.myproject_lesson2.m.myproject_lesson2.R;
import com.myproject_lesson2.m.myproject_lesson2.models.MenuModel;

import java.util.List;



/**
 * Created by M on 11/10/2017.
 */

public class MenuListAdapter extends BaseAdapter {
    Context mcontext;
    List<MenuModel> listMenuModel;

    public MenuListAdapter(Context mcontext, List<MenuModel> listMenuModel) {
        this.mcontext = mcontext;
        this.listMenuModel = listMenuModel;
    }

    @Override
    public int getCount() {
        return listMenuModel.size();
    }

    @Override
    public Object getItem(int position) {
        return listMenuModel.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
       View rowView= LayoutInflater.from(mcontext).inflate(R.layout.menu_list_item,viewGroup,false);
        ImageView imgMenuItem=(ImageView)rowView.findViewById(R.id.imgMenuItem);
        TextView txtMenuName=(TextView)rowView.findViewById(R.id.txtMenuName);
       txtMenuName.setText(listMenuModel.get(position).getMenuName());

        int id =mcontext.getResources().getIdentifier(listMenuModel.get(position).getImgUrl(), "drawable",mcontext.getPackageName());
        imgMenuItem.setImageResource(id);
       return  rowView;
    }
}
