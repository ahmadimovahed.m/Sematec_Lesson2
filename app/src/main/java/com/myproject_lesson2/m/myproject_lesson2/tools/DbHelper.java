package com.myproject_lesson2.m.myproject_lesson2.tools;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.myproject_lesson2.m.myproject_lesson2.models.Condition;

/**
 * Created by M on 11/7/2017.
 */

public class DbHelper extends SQLiteOpenHelper {

    String txtTbWeather="create table TbWeather("
            +"WeatherId INTEGER AUTO INCREMENT PRIMARY KEY, "
            +"CityName text,"
            +"Temp text,"
            +"Date text,"
            +"Textabout text"
            +")";
    public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(txtTbWeather);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void TbWeather_Ins(String CityName, Condition model){
        String strSqldel="delete from TbWeather where CityName="+"'"+CityName+"'";



        String strSql="Insert into TbWeather(CityName,Temp,Date,Textabout)values("+
                "'"+CityName+"','"+
                ""+model.getTemp().toString()+"','"+
                ""+model.getDate().toString()+"','"+
                ""+model.getText().toString()+"')";
        SQLiteDatabase db=this.getWritableDatabase();


        db.execSQL(strSqldel);
        db.execSQL(strSql);
        db.close();
    }


    public Condition TbWeather_Sel(String CityName){
        String strSql="select   CityName,Temp,Date,Textabout from TbWeather  where CityName='"+CityName+"'"
                +" order by WeatherId desc LIMIT 1";
       SQLiteDatabase db=this.getReadableDatabase();
        Cursor item =db.rawQuery(strSql,null);
        Condition model=new Condition();
        while (item.moveToNext()) {
            model.setTemp(item.getString(1));
            model.setDate(item.getString(2));
            model.setText(item.getString(3));

        }
        return model;
    }
}
