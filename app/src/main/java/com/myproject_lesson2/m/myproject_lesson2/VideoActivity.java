package com.myproject_lesson2.m.myproject_lesson2;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        setFinishOnTouchOutside(false);

        String videoUrl="https://as6.cdn.asset.aparat.com/aparat-video/bf4a4a7b471ccd9accb06dfd2af2749a8636843-144p__67863.mp4";
       VideoView videoshow=(VideoView)findViewById(R.id.videoshow);
        videoshow.setMediaController(new MediaController(this));
        videoshow.setVideoURI(Uri.parse(videoUrl));
        videoshow.start();
    }
}
