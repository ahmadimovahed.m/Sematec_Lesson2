
package com.myproject_lesson2.m.myproject_lesson2.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaadrianProductmodel {

    @SerializedName("ProductInfoId")
    @Expose
    private Integer productInfoId;
    @SerializedName("ValidationCode")
    @Expose
    private Object validationCode;
    @SerializedName("SerialNo")
    @Expose
    private Object serialNo;
    @SerializedName("ProductName")
    @Expose
    private Object productName;
    @SerializedName("ProductCode")
    @Expose
    private Object productCode;
    @SerializedName("StartDateShamsi")
    @Expose
    private Object startDateShamsi;
    @SerializedName("EndDateShamsi")
    @Expose
    private Object endDateShamsi;
    @SerializedName("StartDate")
    @Expose
    private Object startDate;
    @SerializedName("EndDate")
    @Expose
    private Object endDate;
    @SerializedName("ProductGroupName")
    @Expose
    private Object productGroupName;
    @SerializedName("BrandName")
    @Expose
    private Object brandName;
    @SerializedName("ModelName")
    @Expose
    private Object modelName;

    public Integer getProductInfoId() {
        return productInfoId;
    }

    public void setProductInfoId(Integer productInfoId) {
        this.productInfoId = productInfoId;
    }

    public Object getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(Object validationCode) {
        this.validationCode = validationCode;
    }

    public Object getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(Object serialNo) {
        this.serialNo = serialNo;
    }

    public Object getProductName() {
        return productName;
    }

    public void setProductName(Object productName) {
        this.productName = productName;
    }

    public Object getProductCode() {
        return productCode;
    }

    public void setProductCode(Object productCode) {
        this.productCode = productCode;
    }

    public Object getStartDateShamsi() {
        return startDateShamsi;
    }

    public void setStartDateShamsi(Object startDateShamsi) {
        this.startDateShamsi = startDateShamsi;
    }

    public Object getEndDateShamsi() {
        return endDateShamsi;
    }

    public void setEndDateShamsi(Object endDateShamsi) {
        this.endDateShamsi = endDateShamsi;
    }

    public Object getStartDate() {
        return startDate;
    }

    public void setStartDate(Object startDate) {
        this.startDate = startDate;
    }

    public Object getEndDate() {
        return endDate;
    }

    public void setEndDate(Object endDate) {
        this.endDate = endDate;
    }

    public Object getProductGroupName() {
        return productGroupName;
    }

    public void setProductGroupName(Object productGroupName) {
        this.productGroupName = productGroupName;
    }

    public Object getBrandName() {
        return brandName;
    }

    public void setBrandName(Object brandName) {
        this.brandName = brandName;
    }

    public Object getModelName() {
        return modelName;
    }

    public void setModelName(Object modelName) {
        this.modelName = modelName;
    }

}
