package com.myproject_lesson2.m.myproject_lesson2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.myproject_lesson2.m.myproject_lesson2.adapters.MenuListAdapter;
import com.myproject_lesson2.m.myproject_lesson2.models.Condition;
import com.myproject_lesson2.m.myproject_lesson2.models.FoodModel;
import com.myproject_lesson2.m.myproject_lesson2.models.MenuModel;
import com.myproject_lesson2.m.myproject_lesson2.models.WeatherModel;
import com.myproject_lesson2.m.myproject_lesson2.tools.DbHelper;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import recievers.BatteryManagerReciever;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class BatteryManagerActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txtBatteryLevel;
    TextView txtWeather;
    TextView txtShamsiDate;
    TextView txtInternet;
    DbHelper db;
    ListView listviewMenu;
    DrawerLayout drawer;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battery_manager);

        Bind();
        BroadcastReceiver BatteryReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int level=intent.getIntExtra(BatteryManager.EXTRA_LEVEL,-1);
                txtBatteryLevel.setText(String.valueOf(level)+"%");
            }
        };
        //Show BatteryState
        IntentFilter BatteryFilter=new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(BatteryReceiver,BatteryFilter);

        //DateShamsi
        PersianDate pdate = new PersianDate();
        //PersianDateFormat pdformater1 = new PersianDateFormat("Y/m/d");
        PersianDateFormat pdformater1 = new PersianDateFormat("j F Y ");
        txtShamsiDate.setText(pdformater1.format(pdate));

        if(checkOnlineState()==true){
            txtInternet.setText("Network State:"+"\n"+"Connected");
            GetDataFromYahoo("tehran");

        }
        else{
            txtInternet.setText("Network State:"+"\n"+"DisConnected");
           Condition model= db.TbWeather_Sel("tehran");
            String Weathertxt=model.getTemp().toString()+" F "+"\n"+ model.getText().toString()+"\n"+ model.getDate();
            txtWeather.setText("OffLine:"+ Weathertxt);
        }
        //Create Menu Drawer
        CreateMenuListItem();

        listviewMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MenuModel clickItem=(MenuModel)adapterView.getItemAtPosition(i);
                String gotoPage=clickItem.getGotoPage().toString();
               Intent intent1=null;
              //  Toast.makeText(ListViewActivity.this,"You Click at:"+aa.toString(),Toast.LENGTH_SHORT).show();
               switch (gotoPage) {
                   case "ListViewActivity":
                       intent1=new Intent(BatteryManagerActivity.this,ListViewActivity.class);
                       break;
                   case "WebServiceActivity":
                       intent1= new Intent(BatteryManagerActivity.this,WebServiceActivity.class);
                       break;
                   case "MainActivity":
                       intent1=new  Intent(BatteryManagerActivity.this,MainActivity.class);
                       break;

                   case "BrowserActivity":
                       intent1=new  Intent(BatteryManagerActivity.this,BrowserActivity.class);
                       break;
                   case "VideoActivity":
                       intent1=new  Intent(BatteryManagerActivity.this,VideoActivity.class);
                       break;

               }
               startActivity(intent1);
                
            }
        });

        findViewById(R.id.btnMenuToolbar).setOnClickListener(this);
        findViewById(R.id.btnFinish).setOnClickListener(this);


    }
    public void Bind(){
        txtBatteryLevel=(TextView)findViewById(R.id.txtBatteryLevel);
        txtWeather=(TextView)findViewById(R.id.txtWeather);
        txtShamsiDate=(TextView)findViewById(R.id.txtShamsiDate);
        txtInternet=(TextView)findViewById(R.id.txtInternet);
        db=new DbHelper(this,"AppDb.db",null,1);
        listviewMenu=(ListView)findViewById(R.id.listviewMenu);
        drawer=(DrawerLayout) findViewById(R.id.drawer);


    }

    public void GetDataFromYahoo(final String cityName){
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"
                +
                cityName + "%2C%20ir%22)&format=json&env=s" +
                "tore%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        AsyncHttpClient client=new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                Toast.makeText(BatteryManagerActivity.this, "-1", Toast.LENGTH_SHORT);
                txtWeather.setText("Weather Read Failed");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                txtWeather.setText( GetTempGson(responseString,cityName));
            }
        });
    }


    public String GetTempGson(String responseString,String cityName){
        String temp="";
        Gson gson=new Gson();
        WeatherModel model=gson.fromJson(responseString,WeatherModel.class);
        Condition modeldb=new Condition();
        modeldb.setTemp(model.getQuery().getResults().getChannel().getItem().getCondition().getTemp().toString());
        modeldb.setText(model.getQuery().getResults().getChannel().getItem().getCondition().getText().toString());
        modeldb.setDate(model.getQuery().getResults().getChannel().getLastBuildDate().toString());

        temp=modeldb.getTemp().toString()+" F "+"\n"+ modeldb.getText().toString()+"\n"+ modeldb.getDate();
       //Save In Database

        db.TbWeather_Ins(cityName,modeldb);

        Toast.makeText(BatteryManagerActivity.this, "Weather Info Has Been Saved", Toast.LENGTH_SHORT).show();

        return  temp;
    }


    public boolean checkOnlineState() {
        ConnectivityManager CManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo NInfo = CManager.getActiveNetworkInfo();
        if (NInfo != null && NInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }



    public void CreateMenuListItem(){


        MenuModel model1=new MenuModel("FoodList","ListViewActivity","imgfoodlist");
        MenuModel model2=new MenuModel("WebService","WebServiceActivity","imgwebservice");
        MenuModel model3=new MenuModel("SharePreference","MainActivity","imgdb");

        MenuModel model4=new MenuModel("WebBrowser","BrowserActivity","imwbrowser");

        MenuModel model5=new MenuModel("ShowVideo","VideoActivity","imgvideo");

        List<MenuModel> listMenuModel=new ArrayList<MenuModel>();
        listMenuModel.add(model1);
        listMenuModel.add(model2);
        listMenuModel.add(model3);
        listMenuModel.add(model4);
        listMenuModel.add(model5);

        MenuListAdapter adapter=new MenuListAdapter(this,listMenuModel);
        listviewMenu.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.btnMenuToolbar){
            drawer.openDrawer(Gravity.LEFT);

        }
        if(view.getId()==R.id.btnFinish){
           new AlertDialog.Builder(BatteryManagerActivity.this).setTitle("Exit")
            .setMessage("Are you sure you want to exit?")
                   .setNegativeButton("Cancel",null)
                   .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            }).create().show();




        }
    }


    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(Gravity.LEFT)){
            drawer.closeDrawer(Gravity.LEFT);
        }
        else
            super.onBackPressed();
    }
}
